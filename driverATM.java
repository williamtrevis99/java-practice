// atm main driver that creates all the objects and calls all functions
// public interface where all functions can be accessed
// Features  = 
// instert pin number
// home screen 
// View balance
// withdraw cash 

import java.util.Scanner;
import java.util.LinkedList;

interface myMethods { // colection of all methods used

    // public field that can be accessed by entire class
    Scanner scnObject = new Scanner(System.in);
    LinkedList<Integer> pins = new LinkedList<Integer>();
    LinkedList<Integer> bankMoney = new LinkedList<Integer>();


    public void enterPin();

    public void validatePin(int refPin);

    public void home(int i);

    public void viewBalance();

    public void withdrawMoney();

}

class atm implements myMethods { // atm class inherits from myMethods

    public int pinNumber;
    public int flag = 1;

    public atm() { // constructor that greets the user

        System.out.println("Welcome to your ATM, please enter your pin to begin.");
        pins.add(2297); // adds values to the Linked List
        pins.add(2298);
        bankMoney.add(20000);

    }

    public void enterPin() {
        System.out.print("PIN: ");
        pinNumber = scnObject.nextInt();
        validatePin(pinNumber);

    }

    public void validatePin(int refPin) { // could of used easy loop but when match found woud couldnt break out

        for (int i : pins) {

            if (i != refPin) {

            } else if (i == refPin) {
                flag = 0;
            }

        }

        switch (flag) { // switch case to detct if a match was found during search
        case 0:
            home(refPin);
            break;
        case 1:
            enterPin();
        }

    }

    public void home(int i) {

        System.out.println("\nHOME\n");
        System.out.println("\n1--> View Balance");
        System.out.println("2--> Withdraw Money");
        System.out.println("3--> Go Home");

        System.out.print("\nselection --- >");

        int x = scnObject.nextInt();
        switch (x) {
        case 1:
            viewBalance();
            home(i);
            break;
        case 2:
            withdrawMoney();
            home(i);
            break;
        case 3:
            enterPin();
            break;

        }

    }

    public void viewBalance() {

        System.out.println("\nBalance = " + bankMoney.get(0));

    }

    public void withdrawMoney() {

        System.out.println("\nPlease enter the amount of money you would like to withdraw");
        System.out.print("AMOUNT: ");
        int withdrawAmount = scnObject.nextInt();
        int var = bankMoney.get(0);
        bankMoney.add(0, var - withdrawAmount);



    }
}

public class driverATM { // main class that will drive the program

    public static void main(String[] args) {

        try {

            atm obj = new atm(); // create object ob type atm
            obj.enterPin(); // call enterPin function

        } catch (Exception e) {

            System.out.println("Exception Caught");

        } finally {
            System.out.println("\nThankyou for using this program");
        }

    }

}
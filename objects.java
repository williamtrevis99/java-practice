
import java.util.LinkedList;

import javax.swing.text.html.HTMLDocument.Iterator;

import java.util.ArrayList;

public class objects {

    public static void main(String[] args) {

        ArrayList<actions> myList = new ArrayList<actions>();

        actions obj1 = new actions("william");
        actions obj2 = new actions("James");

        myList.add(obj1);
        myList.add(obj2);

        System.out.println(myList); // prints the objects identity hashcode.

    }

}

interface myInterface {

    public void drive();

    public void steer();
}

class actions implements myInterface {

    actions(String name) {

    }

    public void drive() {

        System.out.println("The car is driving");

    }

    public void steer() {
        System.out.println("The car is steering");
    }

}
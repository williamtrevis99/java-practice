import java.util.Scanner;


public class vehicle { // static methods / fields are associated with claass, non-static items are associated with objects. 


    // a class is essentially a blueprint for an object, when you want to create an instance of a class
    // you create an object which inherity the properties of the described class. You can inherity properties of 
    //other classes by using the extends keyword. 

    public String name;
    public int capacity;
    public int maxSpeed;


      public static void main (String [] args) {

        car obj = new car("Ford Fiesta", 5, 210);
        obj.display();


        }
    }


class car extends vehicle {



    car(String one,int two,int three) {

        name = one;
        capacity = two;
        maxSpeed = three;
}


    public void display() {

        System.out.println(name);
        System.out.println(capacity);
        System.out.println(maxSpeed);

    }















}
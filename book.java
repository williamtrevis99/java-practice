
// book application will allow you to add books to a single location

import java.util.Scanner;

interface myInterface { // interface to organise functions of program

    public void login();

    public void viewBooks();

    public void addBook();

    // public void removeBook();
    // public void searchBook();
    public void quit();

    public void home();

}

class book { // main class

    public static void main(String[] args) {
        actions myObj = new actions(); // instanciates an actions object called myObj;
        myObj.login();
    }

}

class actions implements myInterface {

    public static String[] myBooks = new String[100];
    int count = 0;

    public void login() {

        final char loginQuestion;
        final Scanner scan = new Scanner(System.in);
        System.out.println("\nWelcome to the Online Book Bank\n");
        System.out.print("Would you like to log in (y/n) ? --> ");
        loginQuestion = scan.next().charAt(0);

        if (('Y') == loginQuestion) {
            home();
        } else {
            System.out.println("Ok, thankyou for using our system, Goodbye");
            quit();
        }

    }

    public void viewBooks() {

        System.out.println("\nBook Total = " + myBooks.length);

        for (int i = 0; i < count; i++) {

            System.out.println("("+ i + ")" + myBooks[i]);

        }

        home();

    }

    public void addBook() {

        Scanner sca = new Scanner(System.in);
        System.out.print("Add Book: ");
        String addedBook = sca.nextLine();
        myBooks[count] = addedBook;
        count++;
        home();

    }

    public void removeBook() {
        Scanner iscan = new Scanner(System.in);
        System.out.println("Insert the index of the book you would like to remove");
        System.out.println("Index: ");
        int indexVal = iscan.nextInt();
        myBooks[indexVal] = "NULL";
        home();
    }

    public void home() {

        Scanner scn = new Scanner(System.in);
        System.out.println("\n(indicate selction by inserting associated number)\n");
        System.out.println("(0) Add book");
        System.out.println("(1) Remove book");
        System.out.println("(2) View Books");
        System.out.println("(3) Exit\n");

        int menuChoice = scn.nextInt();

        switch (menuChoice) {

        case 0:
            addBook();
            break;

        case 1:
             removeBook();

        case 2:
            viewBooks();
            break;

        case 3:
            quit();
            break;

        }

    }

    public void quit() {

        System.exit(0);

    }

}
import java.util.*;
import java.util.HashMap;

// HASHTABLES - A KEY : VALUE LOOKUP SOLUTION

public class hashtables {

    public static void main(String[] args) {

        HashMap<Integer, String> hashtble = new HashMap<Integer, String>();
        hashtble.put(3,"Geeks");
        hashtble.put(5,"hello");

        System.out.println(hashtble.values());
        System.out.println(hashtble.get(3)); // fetches mapped key

    }
}



import java.util.ArrayList;

public class CreateArrayListExample {

    public static void main(String[] args) {

        ArrayList<String> names = new ArrayList<String>();
        names.add("James");

        for (String i : names) {
            System.out.println(i);
        }
    }

}
import java.util.Hashtable;
import java.util.HashMap;
import java.lang.reflect.*;
import java.util.*;

//A core component of any hash table implementation is the hashing function.
//When applied to a key, the hashing function yields an index of an array where
//a record can be stored or retrieved.


public class hashtables {

    public Hashtable<Integer, String> htble = new Hashtable<Integer, String>(); // public class hastable

    public static void main(String[] args) {

        hashtables h = new hashtables();



        h.func1();
        h.func3();
        h.func2();
        h.func3();
        System.out.println(h.hashCode());
        

        

    }

    private void func1() {

        htble.put(1, "William");
        htble.put(2, "Alex");
        htble.put(3, "James");
        System.out.println(htble.get(3)); // retrieve value using key 
        System.out.println(htble.getClass()); 

    }

    private void func2() {

        htble.replace(2, "Alex", "Herbert");
        System.out.println(htble);

    }

    private void func3() {

        boolean x = htble.contains("Alex");
        System.out.println(x);
        System.out.println(htble.get(1)); // access value mapped to the keyValue
        System.out.println(htble.hashCode());

        

    }

    

}


